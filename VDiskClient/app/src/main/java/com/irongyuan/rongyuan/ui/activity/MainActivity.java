package com.irongyuan.rongyuan.ui.activity;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.irongyuan.rongyuan.R;
import com.irongyuan.rongyuan.base.BaseActivity;
import com.irongyuan.rongyuan.bean.TestBean;
import com.irongyuan.rongyuan.net.RequestNetUtils;
import com.irongyuan.rongyuan.utils.DisplayOptionUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    private ImageLoader mImageLoader = ImageLoader.getInstance();
    private DisplayImageOptions mOptions = DisplayOptionUtils.getBigOptions();
    private FloatingActionButton fab;
    private Toolbar toolbar;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
//    }

    @Override
    protected int layoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        super.initView();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        fab = (FloatingActionButton) findViewById(R.id.fab);
    }

    @Override
    protected void initViewListener() {
        super.initViewListener();
        fab.setOnClickListener(this);
    }

    @Override
    protected void doOtherThing() {
        super.doOtherThing();
        setSupportActionBar(toolbar);
    }

    private void getHeaderDetail() {
        ArrayMap<String, String> params = new ArrayMap<>();
        params.put("参数键", "参数值");
        RequestNetUtils.getTest(params, new Response.Listener<TestBean>() {
            @Override
            public void onResponse(TestBean response) {
                Log.i(TAG, "成功,参数response就是返回的数据bean");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
    }

    private void imageLoaderTest(){
        String imageUrl = "";
        ImageView imageView = new ImageView(this);
        mImageLoader.displayImage(imageUrl,imageView,mOptions);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fab:
                getHeaderDetail();
                break;
        }
    }
}
