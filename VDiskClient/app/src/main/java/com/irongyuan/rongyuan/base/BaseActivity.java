
package com.irongyuan.rongyuan.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.irongyuan.rongyuan.R;
import com.irongyuan.rongyuan.utils.ExitAppUtils;

/**
 */
public abstract class BaseActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutId());
        /** 获取intent数据 **/
        getIntentWord();
        /** 初始化控件 **/
        initView();
        /** 控件事件 **/
        initViewListener();
        /** 其他操作 **/
        doOtherThing();
        ExitAppUtils.getInstance().addActivity(this);
    }

    protected abstract int layoutId();

    protected void getIntentWord() {
    }

    protected void initView() {
    }

    protected void initViewListener() {
    }

    protected void doOtherThing() {
    }

    public void anim_right_in() {
        overridePendingTransition(R.anim.slide_right_in, R.anim.stay);
    }

    public void anim_right_out() {
        overridePendingTransition(R.anim.stay, R.anim.slide_right_out);
    }

    public void anim_down_in() {
        overridePendingTransition(R.anim.slide_down_in, R.anim.stay);
    }

    public void anim_stay() {
        overridePendingTransition(R.anim.stay, R.anim.stay);
    }

    public void anim_down_out() {
        overridePendingTransition(R.anim.stay, R.anim.slide_down_out);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        ExitAppUtils.getInstance().delActivity(this);
    }

}
