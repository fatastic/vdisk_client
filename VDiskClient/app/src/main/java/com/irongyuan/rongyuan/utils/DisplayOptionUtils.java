package com.irongyuan.rongyuan.utils;

import android.graphics.Bitmap;

import com.irongyuan.rongyuan.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * DisplayOption 工具类
 * jqh
 */
public class DisplayOptionUtils {
    /**
     * 加载小图的options获取
     *
     * @return
     */
    public static DisplayImageOptions getOptions() {
        return new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .showImageOnLoading(R.drawable.ic_launcher)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .build();
    }

    /**
     * 加载中图的options获取
     *
     * @return
     */
    public static DisplayImageOptions getMiddleOptions() {
        return new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .showImageOnLoading(R.drawable.ic_launcher)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .build();
    }
    /**
     * 加载大图的options获取
     *
     * @return
     */
    public static DisplayImageOptions getBigOptions() {
        return new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .showImageOnLoading(R.drawable.ic_launcher)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .build();
    }

}
