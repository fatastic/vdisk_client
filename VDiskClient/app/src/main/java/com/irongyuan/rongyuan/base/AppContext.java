
package com.irongyuan.rongyuan.base;

import android.app.Application;
import android.content.Context;

import com.irongyuan.rongyuan.net.RequestQueueManager;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;


/**
 */
public class AppContext extends Application {
    private static Context mContext;
    private static String userAgent;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        RequestQueueManager.init(this);
        initImageLoader();
    }


    public static Context getContext() {
        if (mContext == null) {
            throw new IllegalStateException("context is null");
        }
        return mContext;
    }

    /**
     * 配置imageloader
     */
    private void initImageLoader() {
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder(this)
                .threadPriority(Thread.NORM_PRIORITY - 2).denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator()).tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();
        ImageLoader.getInstance().init(configuration);
    }

}
