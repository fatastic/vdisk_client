package com.irongyuan.rongyuan.net;

import android.support.v4.util.ArrayMap;

import com.android.volley.Request;
import com.android.volley.Response;
import com.irongyuan.rongyuan.bean.TestBean;

/**
 * jqh 请求网络工具类
 */
public class RequestNetUtils {
    /**
     *
     * @param params
     * @param listener
     * @param errorListener
     */
    public static void getTest(ArrayMap<String, String> params, Response.Listener<TestBean> listener,
                                               Response.ErrorListener errorListener) {
        GsonRequest<TestBean> req = new GsonRequest<>(Request.Method.POST, UrlManager.URL_TEST,
                params, TestBean.class, listener, errorListener);
        RequestQueueManager.getInstance().addToRequestQueue(req);
    }
}
