
package com.irongyuan.rongyuan.net;

/**
 * 描述:接口api
 *
 */
public class UrlManager {

    /**
     * 预发布地址
     */
    private static final String URL_PRE_RELEASE = "";

    /**
     * 正式线上地址
     */
    private static final String URL_RELEASE = "";

    /**
     * 测试地址http://o.pengyouwan.com:8081
     */
    private static final String URL_DEBUG = "";

    public static final String REQUEST_URL = URL_RELEASE;

    /**
     * 首页头部接口
     */
    public static final String URL_TEST = REQUEST_URL + "home/index";
}
