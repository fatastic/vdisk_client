package com.irongyuan.rongyuan.utils;

import android.app.Activity;
import android.os.Build;

import java.util.LinkedList;
import java.util.List;

/**
 * 类描述:退出应用工具类
 * 作者：jqh
 * 时间：2015/11/27 16:11
 */
public class ExitAppUtils {
    private List<Activity> mActivityList = new LinkedList<Activity>();
    private static ExitAppUtils instance = new ExitAppUtils();

    private ExitAppUtils() {
    }

    /**
     * @return
     */
    public static ExitAppUtils getInstance() {
        return instance;
    }

    /**
     * @param activity
     */
    public void addActivity(Activity activity) {
        mActivityList.add(activity);
    }

    /**
     * @param activity
     */
    public void delActivity(Activity activity) {
        mActivityList.remove(activity);
    }

    /**
     * 退出应用
     */
    public void exit() {
        finishActivities();
		System.exit(0);
    }


    private void finishActivities(){
        for (Activity activity : mActivityList) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(activity!=null && !activity.isDestroyed()) {
                    activity.finish();
                }
            }else{
                if(activity!=null && !activity.isFinishing()) {
                    activity.finish();
                }
            }
        }
    }
}
