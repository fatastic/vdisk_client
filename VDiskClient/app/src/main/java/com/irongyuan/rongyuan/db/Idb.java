package com.irongyuan.rongyuan.db;

/**
 * 类描述:数据库操作抽象类,第一个泛型为查询出来的,第二个为插入的时候传的值
 * 作者：jqh
 * 时间：2015/12/12 11:38
 */
public interface Idb<T,V>  {
    T query();
    void delete();
    long insert(V v);
    long update();
}
